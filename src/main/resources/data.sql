INSERT INTO tbl_users (user_id, last_name, first_name, email, password, enabled, last_password_reset_date) VALUES
    (1,'Doe','John','john.doe@domain.com','$2a$10$rMYh435XAvGHmyeoOiX22.D3r.z.vjNGwdnDc8B6SCBDA4kP/qG.K',true,'2017-10-01 21:58:58.508-07'),
    (2,'Doe','Jane','jane.doe@domain.com','$2a$10$rMYh435XAvGHmyeoOiX22.D3r.z.vjNGwdnDc8B6SCBDA4kP/qG.K',true,'2017-10-01 21:58:58.508-07'),
    (3,'Jones','Cathy','cathy.jones@domain.com','$2a$10$rMYh435XAvGHmyeoOiX22.D3r.z.vjNGwdnDc8B6SCBDA4kP/qG.K',true,'2017-10-01 21:58:58.508-07'),
    (4,'Bond','James','james.bond@domain.com','$2a$10$rMYh435XAvGHmyeoOiX22.D3r.z.vjNGwdnDc8B6SCBDA4kP/qG.K',true,'2017-10-01 21:58:58.508-07'),
    (5,'Williams','Serena','serena.williams@domain.com','$2a$10$rMYh435XAvGHmyeoOiX22.D3r.z.vjNGwdnDc8B6SCBDA4kP/qG.K',true,'2017-10-01 21:58:58.508-07');

INSERT INTO tbl_clients (client_id, client_name, address, email, phone_no) VALUES
    (1,'Zath Corporation','224 Ford Avenue','contactus@zathcorporation.com','(028) 9556-928'),
    (2,'Hattax Marketing','207 Thebes Grove','contactus@hattaxmarketing.com','(021) 2267-937'),
    (3,'Faxtouch Consultancy','216 Tisch Avenue','info@faxtouch.com','(026) 3370-815'),
    (4,'Mediatech Clinic','88 Sawyers Arms Road','info@mediatech.com','(026) 9368-323'),
    (5,'Duoplus Services','119 Pittville Place','contact@duoplus.com','(028) 7668-365'),
    (6,'Linetechno Telecom','110 Ross Grove','askquestion@linetechno.com','(022) 8937-255');

INSERT INTO tbl_projects (project_id, project_name, project_code, description, client_id) VALUES
    (1,'Project A','1.0000001','',1),
    (2,'Projecy B','1.0000002','',1),
    (3,'Project C','1.0000003','',1),
    (4,'Project D','1.0000004','',1),
    (5,'Project E','1.0000005','',2),
    (6,'Project F','1.0000006','',2),
    (7,'Project G','1.0000007','',3),
    (8,'Project H','1.0000008','',3);

INSERT INTO tbl_projects_approvers (project_id, user_id) VALUES
    (1,3), (2,3), (3,3), (4,3);

INSERT INTO tbl_user_clients (user_id, client_id) VALUES
    (1,1), (1,2), (1,3), (2,4), (3,5);

INSERT INTO tbl_roles (role_id, role_name) VALUES
    (1,'ROLE_ADMIN'), (2,'ROLE_USER'), (3, 'ROLE_APPROVER');

INSERT INTO tbl_user_roles (user_id, role_id) VALUES
    (1,1), (2,2), (3,3), (4,2), (5,2);

INSERT INTO tbl_timesheets (timesheet_id, user_id, project_id, timesheet_status) VALUES
    (1,1,1,'DRAFT'),
    (2,1,2,'SUBMITTED');

INSERT INTO tbl_timesheets_logs (timesheet_log_id, timesheet_id, log_date, log_hours) VALUES
    (1,1,'2017-01-01',1),
    (2,1,'2018-01-02',2),
    (3,1,'2018-01-03',3),
    (4,1,'2018-01-04',4),
    (5,1,'2018-01-05',5),
    (6,2,'2017-01-01',8),
    (7,2,'2018-01-02',2),
    (8,2,'2018-01-03',3),
    (9,2,'2018-01-04',4),
    (10,2,'2018-01-05',5);
