package apps.timetify.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import apps.timetify.api.model.User;
import apps.timetify.api.repository.UserRepository;

/**
 *
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 30, 2017
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = repository.findByEmail(username);
        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Email not found!"));
        return optionalUser.get();
    }

}
