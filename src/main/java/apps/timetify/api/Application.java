package apps.timetify.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@SpringBootApplication
@EnableJpaRepositories("apps.timetify.api.repository")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

}
