package apps.timetify.api.mappers;

import apps.timetify.api.dto.TimesheetLogDto;
import apps.timetify.api.model.TimesheetLog;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.WARN
)
public interface TimesheetLogMapper {

    TimesheetLog toModel(TimesheetLogDto timesheetLogDto);

    @InheritInverseConfiguration
    TimesheetLogDto toDto(TimesheetLog timesheetLog);

    TimesheetLog merge(TimesheetLog timesheetLog, @MappingTarget TimesheetLog timesheetLog_2);

}
