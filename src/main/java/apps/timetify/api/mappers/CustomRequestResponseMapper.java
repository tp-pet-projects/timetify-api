package apps.timetify.api.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collections;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
public class CustomRequestResponseMapper extends RequestResponseBodyMethodProcessor {

    private static final Logger LOGGER = Logger.getLogger(CustomRequestResponseMapper.class);

    private final ApplicationContext applicationContext;
    private final EntityManager entityManager;

    public CustomRequestResponseMapper(ApplicationContext applicationContext, EntityManager entityManager, ObjectMapper objectMapper) {
        super(Collections.<HttpMessageConverter<?>>singletonList(new MappingJackson2HttpMessageConverter(objectMapper)));

        this.applicationContext = applicationContext;
        this.entityManager = entityManager;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(ModelMapper.class);
    }

    @Override
    protected <T> Object readWithMessageConverters(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType) throws IOException, HttpMediaTypeNotSupportedException, HttpMessageNotReadableException {
        ModelMapper modelMapper = parameter.getParameterAnnotation(ModelMapper.class);

        if (modelMapper != null) {
            return super.readWithMessageConverters(inputMessage, parameter, modelMapper.sourceType());
        }

        return super.readWithMessageConverters(inputMessage, parameter, targetType);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        ModelMapper modelMapper = parameter.getParameterAnnotation(ModelMapper.class);

        Object dtoObject = super.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
        Object mapper = applicationContext.getBean(modelMapper.mapper());

        Object id = getEntityId(dtoObject);

        LOGGER.debug("DTO object " + dtoObject);

        Method mapperMethod = null;
        Method mergerMethod = null;

        for (Method method : mapper.getClass().getMethods()) {
            if (modelMapper.method().equals(method.getName())) {
                mapperMethod = method;
            } else if ("merge".equals(method.getName())) {
                mergerMethod = method;
            }
        }

        if (mapperMethod == null) {
            throw new IllegalArgumentException("[mapper] parameter is not found.");
        }

        if (id == null) {
            return mapperMethod.invoke(mapper, dtoObject);
        }

        Object convertedObject = mapperMethod.invoke(mapper, dtoObject);
        Object persistentObject = entityManager.find(parameter.getParameterType(), id);

        if (persistentObject != null) {
            if (mergerMethod == null) {
                throw new IllegalArgumentException(String.format("No implementation found for method [merge] in class [%s]", modelMapper.mapper()));
            }

            LOGGER.debug("Persistent object found " + persistentObject);

            return mergerMethod.invoke(mapper, convertedObject, persistentObject);
        } else {
            return null;
        }
    }

    private Object getEntityId(@NotNull Object dto) {
        for (Field field : dto.getClass().getDeclaredFields()) {
            if (field.getAnnotation(Id.class) != null) {
                try {
                    field.setAccessible(true);
                    return field.get(dto);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return null;
    }

}
