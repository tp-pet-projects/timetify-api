package apps.timetify.api.mappers;

import apps.timetify.api.dto.TimesheetDto;
import apps.timetify.api.model.Timesheet;
import org.mapstruct.*;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.WARN,
    uses = {
        TimesheetLogMapper.class
    }
)
public interface TimesheetMapper {

    @Mappings({
        @Mapping(source = "projectId", target = "project.projectId")
    })
    Timesheet toModel(TimesheetDto timesheetDto);

    @InheritInverseConfiguration
    TimesheetDto toDto(Timesheet timesheet);

    Timesheet merge(Timesheet timesheet, @MappingTarget Timesheet timesheet_2);

}
