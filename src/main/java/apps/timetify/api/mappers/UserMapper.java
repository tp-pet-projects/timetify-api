package apps.timetify.api.mappers;

import apps.timetify.api.dto.UserCreationDto;
import apps.timetify.api.dto.UserDto;
import apps.timetify.api.model.User;
import org.mapstruct.*;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserMapper {

    @Mappings({
        @Mapping(target = "authorities", ignore = true),
    })
    User fromUserCreationDto(UserCreationDto userCreationDto);

    @Mappings({
        @Mapping(target = "authorities", ignore = true),
    })
    User toModel(UserDto userDto);

    @InheritInverseConfiguration
    UserDto toDto(User user);

    @Mappings({
        @Mapping(target = "authorities", ignore = true),
    })
    User merge(User user, @MappingTarget User user_2);

}
