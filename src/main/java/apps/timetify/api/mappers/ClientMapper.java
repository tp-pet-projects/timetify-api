package apps.timetify.api.mappers;

import apps.timetify.api.dto.ClientCreationDto;
import apps.timetify.api.dto.ClientDto;
import apps.timetify.api.model.Client;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ClientMapper {

    Client fromClientCreationDto(ClientCreationDto clientCreationDto);
    
    Client toModel(ClientDto clientDto);

    @InheritInverseConfiguration
    ClientDto toDto(Client client);

    Client merge(Client client, @MappingTarget Client client_2);

}
