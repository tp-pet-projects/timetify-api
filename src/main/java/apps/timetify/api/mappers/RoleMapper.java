package apps.timetify.api.mappers;

import apps.timetify.api.dto.RoleDto;
import apps.timetify.api.model.Role;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface RoleMapper {


    Role toModel(RoleDto roleDto);

    @InheritInverseConfiguration
    RoleDto toDto(Role role);

    Role merge(Role role, @MappingTarget Role role_2);

}
