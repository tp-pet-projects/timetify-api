package apps.timetify.api.mappers;

import apps.timetify.api.dto.ProjectCreationDto;
import apps.timetify.api.dto.ProjectDto;
import apps.timetify.api.model.Project;
import apps.timetify.api.model.User;
import org.mapstruct.*;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 07, 2017
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.WARN
)
public interface ProjectMapper {

    @Mappings({
        @Mapping(source = "clientId", target = "client.clientId")
    })
    Project fromProjectCreationDto(ProjectCreationDto projectCreationDto);

    @Mappings({
        @Mapping(source = "clientId", target = "client.clientId")
    })
    Project toModel(ProjectDto projectDto);

    @InheritInverseConfiguration
    @Mapping(source = "client.clientName", target = "clientName")
    ProjectDto toDto(Project project);

    Project merge(Project project, @MappingTarget Project project_2);

    default Set<User> mapToLong(Set<Long> approverIds) {
        return approverIds.stream()
            .map(approverId -> new User(approverId))
            .collect(Collectors.toSet());

    }

    default Set<Long> mapToUser(Set<User> approvers) {
        return approvers.stream()
            .map(user -> user.getUserId())
            .collect(Collectors.toSet());
    }

}
