package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Getter
@Setter
@ToString
public class ClientDto extends ResourceSupport {

    @Id
    @NotNull
    private Long clientId;

    @NotNull
    private String clientName;

    @NotNull
    private String address;

    @NotNull
    private String email;

    @NotNull
    private String phoneNo;

}
