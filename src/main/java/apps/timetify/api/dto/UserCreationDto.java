package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Getter
@Setter
public class UserCreationDto extends ResourceSupport {

    @NotNull
    private String lastName;

    @NotNull
    private String firstName;

    @NotNull
    private String email;

    @NotNull
    private String password;

}
