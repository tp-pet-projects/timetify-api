package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Getter
@Setter
@ToString
public class UserDto extends ResourceSupport {

    @Id
    @NotNull
    private Long userId;

    @NotNull
    private String lastName;

    @NotNull
    private String firstName;

    @NotNull
    private String email;

    private Set<String> userRoles = new HashSet<>();

    public String getFullName() {
        return String.format("%s, %s", lastName, firstName);
    }

}
