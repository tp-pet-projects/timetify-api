package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 31, 2017
 */
@Getter
@Setter
@ToString
public class UserTokenDto {

    private String access_token;
    private Long expires_in;

    public UserTokenDto(String access_token, long expires_in) {
        this.access_token = access_token;
        this.expires_in = expires_in;
    }

}
