package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Getter
@Setter
@ToString
public class ProjectCreationDto extends ResourceSupport {

    @NotNull
    private Long clientId;

    @NotNull
    private String projectName;

    @NotNull
    private String projectCode;

    @NotNull
    private String description;

    @NotEmpty
    private Set<Long> approvers = new HashSet<>();

}
