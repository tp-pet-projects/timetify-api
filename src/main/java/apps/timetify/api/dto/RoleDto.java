package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RoleDto extends ResourceSupport {

    @Id
    @NotNull
    private Long roleId;

    @NotNull
    private String roleName;

}
