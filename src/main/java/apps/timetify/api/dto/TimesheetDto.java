package apps.timetify.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TimesheetDto extends ResourceSupport {

    @Id
    @NotNull
    private Long timesheetId;

    @NotNull
    private Long projectId;

    @NotNull
    private String comments;

    private String timesheetStatus;

    @NotNull
    private Set<TimesheetLogDto> timesheetLogs = new HashSet<>();

}
