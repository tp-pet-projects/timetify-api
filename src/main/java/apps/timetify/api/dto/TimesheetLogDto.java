package apps.timetify.api.dto;

import apps.timetify.api.util.DefaultDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TimesheetLogDto extends ResourceSupport {

    private Long timesheetLogId;

    @NotNull
    private Integer logHours;

    @NotNull
    @JsonSerialize(using = DefaultDateSerializer.class)
    private Date logDate;

}
