package apps.timetify.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Getter
@Setter
@ToString
public class ProjectDto extends ResourceSupport {

    @Id
    @NotNull
    private Long projectId;

    @NotNull
    private Long clientId;

    private String clientName;

    @NotNull
    private String projectName;

    @NotNull
    private String projectCode;

    @NotNull
    private String description;

    @NotEmpty
    private Set<Long> approvers = new HashSet<>();

}
