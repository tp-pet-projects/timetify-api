package apps.timetify.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import apps.timetify.api.model.User;

/**
 *
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
public interface UserRepository extends CrudRepository<User, Long> {

    @EntityGraph(attributePaths = "roles")
    Optional<User> findByEmail(String email);

}
