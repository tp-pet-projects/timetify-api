package apps.timetify.api.repository;

import apps.timetify.api.model.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
public interface ClientRepository extends PagingAndSortingRepository<Client, Long>{
}
