package apps.timetify.api.repository;

import apps.timetify.api.model.Timesheet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
public interface TimesheetRepository extends PagingAndSortingRepository<Timesheet, Long> {

    @Query(value = "SELECT * FROM tbl_timesheets t1 " +
        "JOIN tbl_projects t2 ON t2.project_id = t1.project_id " +
        "JOIN tbl_clients t3 ON t3.client_id = t2.client_id " +
        "LEFT JOIN tbl_timesheets_logs t4 ON t4.timesheet_id = t1.timesheet_id " +
        "WHERE t3.client_id = :clientId " +
        "AND t1.user_id = :userId " +
        "AND t4.log_date BETWEEN :start AND :end " +
        "ORDER BY t1.timesheet_id",
        nativeQuery = true)
    List<Timesheet> findTimesheetLogs(@Param("userId") Long userId,
                                      @Param("clientId") Long clientId,
                                      @Param("start") Date start,
                                      @Param("end") Date end);

}
