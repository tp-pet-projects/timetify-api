package apps.timetify.api.repository;

import apps.timetify.api.model.Project;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    @EntityGraph(attributePaths = "approvers")
    @Query(value = "SELECT t FROM Project t WHERE t.projectId = :id")
    Project findProjectApprovers(@Param("id") Long projectId);

}
