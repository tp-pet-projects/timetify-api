package apps.timetify.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Entity
@Table(name = "tbl_users")
@Getter
@Setter
@ToString(exclude = { "clients", "roles", "timesheets", "assignedProjects" })
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userIdGenerator")
    @SequenceGenerator(name = "userIdGenerator", sequenceName = "tbl_users_seq")
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "email")
    private String email;

    @Column(name = "password", length = 100)
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "last_password_reset_date")
    private Timestamp lastPasswordResetDate;

    @ManyToMany(fetch = FetchType.LAZY,
        cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
        })
    @JoinTable(name = "tbl_user_clients",
        joinColumns = {
            @JoinColumn(name = "user_id", table = "tbl_users")
        }, inverseJoinColumns = {
        @JoinColumn(name = "client_id", table = "tbl_clients")
    })
    @OrderBy(value = "client_name")
    private Set<Client> clients = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
        cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
        })
    @JoinTable(name = "tbl_user_roles",
        joinColumns = {
            @JoinColumn(name = "user_id", table = "tbl_users")
        }, inverseJoinColumns = {
        @JoinColumn(name = "role_id", table = "tbl_roles")
    })
    @OrderBy(value = "role_name")
    private Set<Role> roles = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY,
        cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
        }, mappedBy = "user")
    private Set<Timesheet> timesheets = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "approvers")
    private Set<Project> assignedProjects = new HashSet<>();

    public User() {
    }

    public User(Long userId) {
        this.userId = userId;
    }

    @Override
    public String getUsername() {
        return this.getEmail();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.getRoles()
            .stream().map(role -> {
                return new SimpleGrantedAuthority(role.getRoleName());
            }).collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
