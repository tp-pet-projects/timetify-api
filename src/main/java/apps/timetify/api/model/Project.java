package apps.timetify.api.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@Entity
@Table(name = "tbl_projects")
@Data
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projectIdGenerator")
    @SequenceGenerator(name = "projectIdGenerator", sequenceName = "tbl_projects_seq")
    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "project_code")
    private String projectCode;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "timesheet_id")
    private Timesheet timesheet;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tbl_projects_approvers",
        joinColumns = {
            @JoinColumn(name = "project_id", table = "tbl_projects")
        },
        inverseJoinColumns = {
            @JoinColumn(name = "user_id", table = "tbl_users")
        })
    @OrderBy(value = "last_name")
    private Set<User> approvers = new HashSet<>();

}
