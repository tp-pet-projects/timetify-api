package apps.timetify.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
@Entity
@Table(name = "tbl_timesheets_logs")
@Getter
@Setter
@ToString(exclude = { "timesheet" })
public class TimesheetLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timesheetLogIdGenerator")
    @SequenceGenerator(name = "timesheetLogIdGenerator", sequenceName = "tbl_timesheet_log_seq")
    @Column(name = "timesheet_log_id")
    private Long timesheetLogId;


    @Column(name = "log_date")
    @Temporal(value = TemporalType.DATE)
    private Date logDate;

    @Column(name = "log_hours")
    private Integer logHours;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "timesheet_id")
    private Timesheet timesheet;

}
