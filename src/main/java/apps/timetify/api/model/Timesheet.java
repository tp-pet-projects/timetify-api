package apps.timetify.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
@Entity
@Table(name = "tbl_timesheets")
@Getter
@Setter
@ToString(exclude = { "user", "project" })
public class Timesheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timesheetIdGenerator")
    @SequenceGenerator(name = "timesheetIdGenerator", sequenceName = "tbl_timesheet_seq")
    @Column(name = "timesheet_id")
    private Long timesheetId;

    @Column(name = "comments")
    private String comments;

    @Column(name = "timesheet_status")
    private String timesheetStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @OneToMany(fetch = FetchType.LAZY,
        cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
        }, mappedBy = "timesheet")
    @OrderBy("log_date")
    private Set<TimesheetLog> timesheetLogs = new HashSet<>();

}
