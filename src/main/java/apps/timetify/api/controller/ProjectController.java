package apps.timetify.api.controller;

import apps.timetify.api.dto.ProjectCreationDto;
import apps.timetify.api.dto.ProjectDto;
import apps.timetify.api.dto.UserDto;
import apps.timetify.api.mappers.ModelMapper;
import apps.timetify.api.mappers.ProjectMapper;
import apps.timetify.api.mappers.UserMapper;
import apps.timetify.api.model.Project;
import apps.timetify.api.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping(path = "/{id}")
    public HttpEntity<ProjectDto> getById(@PathVariable Long id) {
        Project project = projectRepository.findOne(id);

        if (project == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(projectMapper.toDto(project));
    }

    @GetMapping
    public HttpEntity<List<ProjectDto>> getList() {
        List<Project> projects = (List<Project>) projectRepository.findAll();

        return ResponseEntity.ok(projects.stream()
            .map(project -> projectMapper.toDto(project))
            .collect(Collectors.toList()));
    }

    @GetMapping(value = "/{id}/approvers")
    public HttpEntity<List<UserDto>> getApprovers(@PathVariable Long id) {
        Project project = projectRepository.findProjectApprovers(id);

        if (project == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(project.getApprovers().stream()
            .map(user -> userMapper.toDto(user))
            .collect(Collectors.toList()));
    }

    @PostMapping
    public HttpEntity<ProjectDto> add(@ModelMapper(sourceType = ProjectCreationDto.class, mapper = ProjectMapper.class, method = "fromProjectCreationDto") Project project) {
        return ResponseEntity.ok(projectMapper.toDto(projectRepository.save(project)));
    }

    @DeleteMapping(path = "/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        if (!projectRepository.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        projectRepository.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public HttpEntity<ProjectDto> update(@ModelMapper(sourceType = ProjectDto.class, mapper = ProjectMapper.class) Project project) {
        if (project == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(projectMapper.toDto(projectRepository.save(project)));
    }

}
