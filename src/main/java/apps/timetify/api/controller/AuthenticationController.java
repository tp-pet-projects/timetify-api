package apps.timetify.api.controller;

import apps.timetify.api.dto.UserTokenDto;
import apps.timetify.api.security.JwtAuthenticationRequest;
import apps.timetify.api.util.TokenHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 31, 2017
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenHelper tokenHelper;

    @PostMapping(value = "/token", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserTokenDto> authenticate(@RequestBody JwtAuthenticationRequest authenticationRequest,
                                                     HttpServletResponse response, Device device) {
        // @formatter:off

        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
                authenticationRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails user = (UserDetails) authentication.getPrincipal();
        return ResponseEntity.ok(new UserTokenDto(
            tokenHelper.generateToken(user.getUsername(), device),
            tokenHelper.getExpiredIn(device)));
    }

}
