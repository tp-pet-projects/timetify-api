package apps.timetify.api.controller;


import apps.timetify.api.dto.ClientCreationDto;
import apps.timetify.api.dto.ClientDto;
import apps.timetify.api.dto.ProjectDto;
import apps.timetify.api.mappers.ClientMapper;
import apps.timetify.api.mappers.ModelMapper;
import apps.timetify.api.mappers.ProjectMapper;
import apps.timetify.api.model.Client;
import apps.timetify.api.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private ClientRepository repository;

    @GetMapping(path = "/{id}")
    public HttpEntity<ClientDto> getById(@PathVariable Long id) {
        Client client = repository.findOne(id);

        if (client == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(clientMapper.toDto(client), HttpStatus.OK);
    }

    @GetMapping
    public HttpEntity<List<ClientDto>> getList() {
        List<Client> clients = (List<Client>) repository.findAll();

        return ResponseEntity.ok(clients.stream()
            .map(client -> clientMapper.toDto(client))
            .collect(Collectors.toList()));
    }

    @GetMapping(path = "/{id}/projects")
    public HttpEntity<List<ProjectDto>> getUserClients(@PathVariable Long id) {
        Client client = repository.findOne(id);

        if (client == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(client.getProjects().stream()
            .map(project -> projectMapper.toDto(project))
            .collect(Collectors.toList()));
    }

    @PostMapping
    public HttpEntity<ClientDto> add(@ModelMapper(sourceType = ClientCreationDto.class, mapper = ClientMapper.class, method = "fromClientCreationDto") Client client) {
        return ResponseEntity.ok(clientMapper.toDto(repository.save(client)));
    }

    @DeleteMapping(path = "/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        if (!repository.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        repository.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public HttpEntity<ClientDto> update(@ModelMapper(sourceType = ClientDto.class, mapper = ClientMapper.class) Client client) {
        if (client == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(clientMapper.toDto(repository.save(client)));
    }

}
