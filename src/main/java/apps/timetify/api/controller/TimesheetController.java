package apps.timetify.api.controller;

import apps.timetify.api.dto.TimesheetDto;
import apps.timetify.api.mappers.ModelMapper;
import apps.timetify.api.mappers.TimesheetMapper;
import apps.timetify.api.model.Timesheet;
import apps.timetify.api.model.User;
import apps.timetify.api.repository.TimesheetRepository;
import apps.timetify.api.security.TokenBasedAuthentication;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Jan 05, 2018
 */
@RestController
@RequestMapping("/timesheets")
public class TimesheetController {

    private static final Logger LOGGER = Logger.getLogger(TimesheetController.class);

    @Autowired
    private TimesheetMapper timesheetMapper;

    @Autowired
    private TimesheetRepository timesheetRepository;

    @GetMapping("/logs/{clientId}")
    public ResponseEntity<List<TimesheetDto>> getList(TokenBasedAuthentication authenticatedUser,
                                                      @PathVariable Long clientId,
                                                      @RequestParam(value = "start") String start,
                                                      @RequestParam(value = "end") String end) throws ParseException {
        User user = (User) authenticatedUser.getPrincipal();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        return ResponseEntity.ok(timesheetRepository.findTimesheetLogs(user.getUserId(), clientId, sdf.parse(start), sdf.parse(end))
            .stream().map(timesheet -> timesheetMapper.toDto(timesheet))
            .collect(Collectors.toList()));
    }


    @PostMapping(value = "/save")
    public ResponseEntity<TimesheetDto> save(TokenBasedAuthentication authenticatedUser,
                                             @ModelMapper(sourceType = TimesheetDto.class, mapper = TimesheetMapper.class) Timesheet timesheet) {
        if (timesheet == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = (User) authenticatedUser.getPrincipal();

        timesheet.setUser(user);
        timesheet.setTimesheetStatus("DRAFT");

        timesheet.getTimesheetLogs().forEach(_log -> {
            _log.setTimesheet(timesheet);
        });

        return ResponseEntity.ok(timesheetMapper.toDto(timesheetRepository.save(timesheet)));
    }

    @PostMapping(value = "/submit")
    public ResponseEntity<TimesheetDto> submit(TokenBasedAuthentication authenticatedUser,
                                               @ModelMapper(sourceType = TimesheetDto.class, mapper = TimesheetMapper.class) Timesheet timesheet) {
        if (timesheet == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = (User) authenticatedUser.getPrincipal();

        timesheet.setUser(user);
        timesheet.setTimesheetStatus("SUBMITTED");

        timesheet.getTimesheetLogs().forEach(_log -> {
            _log.setTimesheet(timesheet);
        });

        return ResponseEntity.ok(timesheetMapper.toDto(timesheetRepository.save(timesheet)));
    }

}
