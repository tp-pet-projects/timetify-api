package apps.timetify.api.controller;

import apps.timetify.api.dto.ClientDto;
import apps.timetify.api.dto.UserDto;
import apps.timetify.api.mappers.ClientMapper;
import apps.timetify.api.mappers.UserMapper;
import apps.timetify.api.model.User;
import apps.timetify.api.repository.UserRepository;
import apps.timetify.api.security.TokenBasedAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public HttpEntity<UserDto> getUser(TokenBasedAuthentication authenticatedUser) {
        User user = userRepository.findOne(((User) authenticatedUser.getPrincipal()).getUserId());

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        UserDto userDto = userMapper.toDto(user);

        userDto.setUserRoles(user.getAuthorities().stream()
            .map(role -> role.getAuthority())
            .collect(Collectors.toSet()));

        return ResponseEntity.ok(userDto);
    }

    @GetMapping(path = "/clients")
    public HttpEntity<List<ClientDto>> getUserClients(TokenBasedAuthentication authenticatedUser) {
        User user = userRepository.findOne(((User) authenticatedUser.getPrincipal()).getUserId());

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(user.getClients().stream()
            .map(client -> clientMapper.toDto(client))
            .collect(Collectors.toList()));
    }

}
