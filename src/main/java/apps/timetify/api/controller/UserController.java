package apps.timetify.api.controller;

import apps.timetify.api.dto.ClientDto;
import apps.timetify.api.dto.RoleDto;
import apps.timetify.api.dto.UserCreationDto;
import apps.timetify.api.dto.UserDto;
import apps.timetify.api.mappers.ClientMapper;
import apps.timetify.api.mappers.ModelMapper;
import apps.timetify.api.mappers.RoleMapper;
import apps.timetify.api.mappers.UserMapper;
import apps.timetify.api.model.Client;
import apps.timetify.api.model.User;
import apps.timetify.api.repository.ClientRepository;
import apps.timetify.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 22, 2017
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "/{id}")
    public HttpEntity<UserDto> getById(@PathVariable Long id) {
        User user = userRepository.findOne(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(userMapper.toDto(user));
    }

    @GetMapping
    public HttpEntity<List<UserDto>> getList() {
        List<User> users = (List<User>) userRepository.findAll();

        return ResponseEntity.ok(users.stream()
            .map(user -> userMapper.toDto(user))
            .collect(Collectors.toList()));
    }

    @GetMapping(path = "/{id}/clients")
    public HttpEntity<List<ClientDto>> getUserClients(@PathVariable Long id) {
        User user = userRepository.findOne(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(user.getClients().stream()
            .map(client -> clientMapper.toDto(client))
            .collect(Collectors.toList()));
    }

    @GetMapping(path = "/{id}/roles")
    public HttpEntity<List<RoleDto>> getUserRoles(@PathVariable Long id) {
        User user = userRepository.findOne(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(user.getRoles().stream()
            .map(role -> roleMapper.toDto(role))
            .collect(Collectors.toList()));
    }

    @PutMapping(path = "/{id}/assign")
    public HttpEntity<UserDto> assignToClient(@PathVariable Long id, @RequestBody List<Long> clientIds) {
        User user = userRepository.findOne(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (CollectionUtils.isEmpty(clientIds)) {
            return ResponseEntity.ok(userMapper.toDto(user));
        }

        clientIds.stream().forEach(clientId -> {
            Client client = clientRepository.findOne(clientId);

            if (client != null) {
                user.getClients().add(client);
            }
        });

        return ResponseEntity.ok(userMapper.toDto(userRepository.save(user)));
    }

    @PostMapping
    public HttpEntity<UserDto> add(@ModelMapper(sourceType = UserCreationDto.class, mapper = UserMapper.class, method = "fromUserCreationDto") User user) {
        return ResponseEntity.ok(userMapper.toDto(userRepository.save(user)));
    }

    @DeleteMapping(path = "/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        if (!userRepository.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userRepository.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public HttpEntity<UserDto> update(@ModelMapper(sourceType = UserDto.class, mapper = UserMapper.class) User user) {
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(userMapper.toDto(userRepository.save(user)));
    }

}
