package apps.timetify.api.security;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Patrick Tan <tanpatrickjoseph@gmail.com>
 * @since Dec 31, 2017
 */
@Getter
@Setter
@ToString
public class JwtAuthenticationRequest {

    private String email;
    private String password;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

}
